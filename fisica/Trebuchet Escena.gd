extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(event):
	if (event.is_action_pressed("lanzar")):
		$Trebuchet/VehicleBody/StaticBody.queue_free()
	if (event.is_action_pressed("liberar")):
		$Trebuchet/rope/HingeJoint22.queue_free()
	if (event.is_action_pressed("acelerar")):
		$Trebuchet/VehicleBody.engine_force = 300;
		$Trebuchet/VehicleBody.brake = 0;
	if (event.is_action_pressed("frenar")):
		$Trebuchet/VehicleBody.engine_force = 0;
		$Trebuchet/VehicleBody.brake = 4;
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
