extends RigidBody

export var velocidad = 20
export var ray_length = 100
onready var ropescene = load("res://rope.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	if (Input.is_action_pressed("forward")):
		self.translate(Vector3(velocidad * delta ,0, 0))
	if (Input.is_action_pressed("backward")):
		self.translate(Vector3(- velocidad * delta ,0, 0))
	if (Input.is_action_pressed("right")):
		self.translate(Vector3(0 , 0 , velocidad * delta ))
	if (Input.is_action_pressed("left")):
		self.translate(Vector3(0 , 0 , - velocidad * delta ))

func _input(event):
	if (event.is_action_pressed("lanzar")):
		var from = $Camera.project_ray_origin(event.position)
		var to = from + $Camera.project_ray_normal(event.position) * ray_length
		var space_state = get_world().direct_space_state
		var selection = space_state.intersect_ray(from, to)
		var empty = {}
		if (!selection.empty()) :
			var rope = ropescene.instance()
			get_parent().add_child(rope)
			rope.translate(self.translation)
			var jointconobj = rope.get_node("HingeJoint2")
			var jointconjugador = rope.get_node("HingeJoint22")
			jointconobj.set_node_a(selection.collider.name)
			jointconjugador.set_node_b("Jugador")
			jointconobj.translate(to)
			jointconjugador.translate(self.translation)
		


