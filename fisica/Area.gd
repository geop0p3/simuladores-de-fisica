extends Area

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var fuerza = 2

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area_body_entered(body):
	print (body.translation)
	var dir = body.translation - self.translation
	body.apply_central_impulse(dir * fuerza)
	pass # Replace with function body.
