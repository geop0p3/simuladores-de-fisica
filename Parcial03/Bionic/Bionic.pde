import shiffman.box2d.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.joints.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;
import org.jbox2d.dynamics.contacts.*;

Box2DProcessing box2d;
Jugador jugador;
Piso plataforma1;
Piso techo;
Piso plataforma2;
Piso plataforma3;
Piso plataforma4;
Piso plataforma5;
Pico pico1A;
Pico pico2A;
Pico pico3A;
Pico pico4A;
Pico pico5A;

RopeJoint jugador_con_mouse;
DistanceJoint garrocha;
WeldJoint garrocha_piso;

Vec2 fuerza_derecha = new Vec2(10,0);
Vec2 fuerza_izquierda = new Vec2(-10,0);

void setup () {
  size (1000, 600);
  smooth();
  box2d = new Box2DProcessing(this);
  box2d.createWorld();
  jugador = new Jugador(15,130, 20, 20);
  plataforma1 = new Piso(400, 150, 800, 10);
  plataforma2 = new Piso(600, 280, 800, 10);
  plataforma3 = new Piso(200, 365, 400, 10);
  plataforma4 = new Piso(320, 465, 180, 10);
  plataforma5 = new Piso(500, 580, 100, 10);
  techo = new Piso(400, 5, 800, 10);
  pico1A = new Pico (100, 80, 115, 45, 130, 80);
  pico2A = new Pico (130, 80, 145, 45, 160, 80);
  pico3A = new Pico (160, 80, 175, 45, 190, 80);
  pico4A = new Pico (190, 80, 205, 45, 220, 80);
  pico5A = new Pico (220, 80, 235, 45, 250, 80);
}

void draw () {
  background(255);
  box2d.step();
  
  jugador.display(35,35,154);
  plataforma1.display(0, 0, 0);
  techo.display(100, 0 ,0);
  pico1A.display(100, 100, 255);
  pico2A.display(100, 100, 255);
  pico3A.display(100, 100, 255);
  pico4A.display(100, 100, 255);
  pico5A.display(100, 100, 255);
  plataforma3.display(100,232,32);
  plataforma4.display(100,132,232);
  plataforma5.display(200,32,232);
  
  if (jugador_con_mouse != null) {
      Vec2 v1 = new Vec2(0,0);
      Vec2 v2 = new Vec2(0,0);
      jugador_con_mouse.getAnchorA(v1);
      jugador_con_mouse.getAnchorB(v2);
      v1 = box2d.coordWorldToPixels(v1);
      v2 = box2d.coordWorldToPixels(v2);
      pushMatrix();
      fill(0,0,0);
      stroke(0,0,0);
      strokeWeight(1.5);
      line(v1.x, v1.y, v2.x, v2.y);
      popMatrix();
    }
  plataforma2.display(0, 0, 0);
  
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == RIGHT) {
      if (jugador_con_mouse == null) {
        if(jugador.body.getLinearVelocity().x < 1){
          jugador.body.setLinearVelocity(fuerza_derecha);
        }
      }
    }
    if (keyCode == LEFT) {
      if (jugador_con_mouse == null) {
        if(jugador.body.getLinearVelocity().x > 1){
          jugador.body.setLinearVelocity(fuerza_izquierda);
        }
      }
    }
  }
}

void mousePressed() {
  if (mouseButton == LEFT){
    if (techo.contains(mouseX,mouseY) && box2d.getBodyPixelCoord(jugador.body).y > box2d.getBodyPixelCoord(techo.body).y){
      RopeJointDef rjd = new RopeJointDef();
      rjd.bodyA = jugador.body;
      rjd.bodyB = techo.body;
      rjd.localAnchorA.x = 0;
      rjd.localAnchorA.y = 0;
      rjd.localAnchorB.x = box2d.scalarPixelsToWorld(mouseX) - box2d.scalarPixelsToWorld(400);
      rjd.localAnchorB.y = 0;
      rjd.maxLength = 7;
      jugador_con_mouse = (RopeJoint) box2d.world.createJoint(rjd);
    }
    
    if (plataforma1.contains(mouseX,mouseY) &&  box2d.getBodyPixelCoord(jugador.body).y > box2d.getBodyPixelCoord(plataforma1.body).y){
      RopeJointDef rjd = new RopeJointDef();
      rjd.bodyA = jugador.body;
      rjd.bodyB = plataforma1.body;
      rjd.localAnchorA.x = 0;
      rjd.localAnchorA.y = 0;
      rjd.localAnchorB.x = box2d.scalarPixelsToWorld(mouseX) - box2d.scalarPixelsToWorld(400);
      rjd.localAnchorB.y = 0;
      rjd.maxLength = 7;
      jugador_con_mouse = (RopeJoint) box2d.world.createJoint(rjd);
    }
    
    if (plataforma2.contains(mouseX,mouseY) &&  box2d.getBodyPixelCoord(jugador.body).y > box2d.getBodyPixelCoord(plataforma2.body).y){
      RopeJointDef rjd = new RopeJointDef();
      rjd.bodyA = jugador.body;
      rjd.bodyB = plataforma2.body;
      rjd.localAnchorA.x = 0;
      rjd.localAnchorA.y = 0;
      rjd.localAnchorB.x = box2d.scalarPixelsToWorld(mouseX) - box2d.scalarPixelsToWorld(600);
      rjd.localAnchorB.y = 0;
      rjd.maxLength = 7;
      jugador_con_mouse = (RopeJoint) box2d.world.createJoint(rjd);
    }
    
    if (plataforma3.contains(mouseX,mouseY) &&  box2d.getBodyPixelCoord(jugador.body).y > box2d.getBodyPixelCoord(plataforma3.body).y){
      RopeJointDef rjd = new RopeJointDef();
      rjd.bodyA = jugador.body;
      rjd.bodyB = plataforma3.body;
      rjd.localAnchorA.x = 0;
      rjd.localAnchorA.y = 0;
      rjd.localAnchorB.x = box2d.scalarPixelsToWorld(mouseX) - box2d.scalarPixelsToWorld(200);
      rjd.localAnchorB.y = 0;
      rjd.maxLength = 7;
      jugador_con_mouse = (RopeJoint) box2d.world.createJoint(rjd);
    }
    
    if (plataforma4.contains(mouseX,mouseY) &&  box2d.getBodyPixelCoord(jugador.body).y > box2d.getBodyPixelCoord(plataforma4.body).y){
      RopeJointDef rjd = new RopeJointDef();
      rjd.bodyA = jugador.body;
      rjd.bodyB = plataforma4.body;
      rjd.localAnchorA.x = 0;
      rjd.localAnchorA.y = 0;
      rjd.localAnchorB.x = box2d.scalarPixelsToWorld(mouseX) - box2d.scalarPixelsToWorld(320);
      rjd.localAnchorB.y = 0;
      rjd.maxLength = 7;
      jugador_con_mouse = (RopeJoint) box2d.world.createJoint(rjd);
    }
    
  }
  if(mouseButton == RIGHT) {
    if (plataforma1.contains(mouseX,mouseY)){
      DistanceJointDef rjd = new DistanceJointDef();
      rjd.bodyA = jugador.body;
      rjd.bodyB = plataforma1.body;
      rjd.localAnchorA.x = 0;
      rjd.localAnchorA.y = 0;
      rjd.localAnchorB.x = box2d.scalarPixelsToWorld(mouseX) - box2d.scalarPixelsToWorld(400);
      rjd.localAnchorB.y = 0;
      rjd.length = 7;
      rjd.frequencyHz = 3;
      rjd.dampingRatio = 0.1;
      garrocha = (DistanceJoint) box2d.world.createJoint(rjd);
    }
    if (plataforma2.contains(mouseX,mouseY)){
      DistanceJointDef rjd = new DistanceJointDef();
      rjd.bodyA = jugador.body;
      rjd.bodyB = plataforma2.body;
      rjd.localAnchorA.x = 0;
      rjd.localAnchorA.y = 0;
      rjd.localAnchorB.x = box2d.scalarPixelsToWorld(mouseX) - box2d.scalarPixelsToWorld(600);
      rjd.localAnchorB.y = 0;
      rjd.length = 7;
      rjd.frequencyHz = 3;
      rjd.dampingRatio = 0.1;
      garrocha = (DistanceJoint) box2d.world.createJoint(rjd);
    }
  }
}

void mouseReleased() {
  if (mouseButton == LEFT) {
    if (jugador_con_mouse != null) {
      box2d.world.destroyJoint(jugador_con_mouse);
      jugador_con_mouse = null;  
    }
  }
  
  if (mouseButton == RIGHT) {
    if (garrocha != null) {
      box2d.world.destroyJoint(garrocha);
      garrocha = null;  
    }
    if (garrocha_piso != null) {
      box2d.world.destroyJoint(garrocha_piso);
      garrocha_piso = null;  
    }
  }
}

void keyReleased() {
  if (jugador_con_mouse == null) {
    jugador.body.setLinearVelocity(new Vec2(0, 0));
  }
}
