class Pico {
  Body body;
  float x1,x2,x3,y1,y2,y3;
  FixtureDef fd = new FixtureDef();
  
  Pico (float _x1, float _y1, float _x2, float _y2, float _x3, float _y3) {
    x1 = _x1;
    x2 = _x2;
    x3 = _x3;
    
    y1 = _y1;
    y2 = _y2;
    y3 = _y3;
    
    float avgx = x1 + x2 + x3;
    float avgy = y1 + y2 + y3;
    
    avgx = avgx / 3;
    avgy = avgy / 3;
    
    BodyDef bd = new BodyDef();
    bd.type = BodyType.STATIC;
    bd.position.set(box2d.coordPixelsToWorld(avgx, avgy));

    body = box2d.createBody(bd);

    Vec2[] vertices = new Vec2 [3];
    vertices[0] = box2d.vectorPixelsToWorld(new Vec2 (x1, y1));
    vertices[1] = box2d.vectorPixelsToWorld(new Vec2 (x2, y2));
    vertices[2] = box2d.vectorPixelsToWorld(new Vec2 (x3, y3));

    PolygonShape ps = new PolygonShape();
    ps.set(vertices, vertices.length);
    fd.shape = ps;

    fd.density = 1;
    fd.friction = 0.3;
    fd.restitution = 0;

    body.createFixture(fd);
  }
  
  void display(float r, float g, float b) {
    Vec2 pos = box2d.getBodyPixelCoord(body);
    float a = body.getAngle();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(-a);
    noStroke();
    fill(r, g, b);
    triangle(x1, y1, x2, y2, x3, y3);
    popMatrix();
  }
}
