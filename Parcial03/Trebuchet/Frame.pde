class Frame {
  Body body;
  float x,y,w,h;
  FixtureDef fd = new FixtureDef();

  Frame (float _x, float _y, float _w, float _h) {
    x = _x;
    y = _y;
    w = _w;
    h = _h;

    BodyDef bd = new BodyDef();
    bd.type = BodyType.DYNAMIC;
    bd.position.set(box2d.coordPixelsToWorld(x, y));

    body = box2d.createBody(bd);

    PolygonShape ps = new PolygonShape();
    float bodylength = box2d.scalarPixelsToWorld(w/2);
    float bodyheight = box2d.scalarPixelsToWorld(h/2);
    ps.setAsBox(bodylength, bodyheight);
    fd.shape = ps;

    fd.density = 1;
    fd.friction = 0;
    fd.restitution = 0;

    body.createFixture(fd);
  }

  void display(float r, float g, float b) {
    Vec2 pos = box2d.getBodyPixelCoord(body);
    float a = body.getAngle();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(-a);
    noStroke();
    fill(r, g, b);
    rectMode(CENTER);
    rect(0, 0, w, h);
    popMatrix();
  }
}
