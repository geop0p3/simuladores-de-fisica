class Piso {
  Body body;
  float x,y,w,h;
  FixtureDef fd = new FixtureDef();
  
  Piso () {
    x = 960;
    y = 925;
    w = 1920;
    h = 15;
    
    BodyDef bd = new BodyDef();
    bd.type = BodyType.STATIC;
    bd.position.set(box2d.coordPixelsToWorld(x, y));

    body = box2d.createBody(bd);

    PolygonShape ps = new PolygonShape();
    float bodylength = box2d.scalarPixelsToWorld(w/2);
    float bodyheight = box2d.scalarPixelsToWorld(h/2);
    ps.setAsBox(bodylength, bodyheight);
    fd.shape = ps;

    fd.density = 1;
    fd.friction = 0.3;
    fd.restitution = 0.5;

    body.createFixture(fd);
  }
  
  void display() {
    Vec2 pos = box2d.getBodyPixelCoord(body);
    float a = body.getAngle();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(-a);
    noStroke();
    fill(105, 105, 105);
    rectMode(CENTER);
    rect(0, 0, w, h);
    popMatrix();
  }
}
