import shiffman.box2d.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.joints.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;
import org.jbox2d.dynamics.contacts.*;
import org.jbox2d.collision.*;

Box2DProcessing box2d;

Piso piso;
Soporte soporte;
Barra barra;
Proyectil proyectil;
Peso peso;
Frame frame;
Rueda rueda1;
Rueda rueda2;

WeldJoint barra_con_peso;
WeldJoint proyectil_con_frame;
WeldJoint soporte_con_frame;
WeldJoint rueda1_con_piso;
WeldJoint rueda2_con_piso;
RopeJoint proyectil_con_barra;
RevoluteJoint rueda1_con_frame;
RevoluteJoint rueda2_con_frame;
RevoluteJoint soporte_con_barra;

RayCastInput inputRD;
RayCastOutput outputRD;
Vec2 c1D,c2D;

Ray r1, r2, r3, r4, r5, r6, r7, r8;

int force = 200000;

void setup () {
  size (1920, 1080);
  smooth();
  box2d = new Box2DProcessing(this);
  box2d.createWorld();
  piso = new Piso();
  soporte = new Soporte();
  barra = new Barra();
  peso = new Peso();
  proyectil = new Proyectil();
  frame = new Frame(420,880, 400,20);
  rueda1 = new Rueda(610,880,25);
  rueda2 = new Rueda(230,880,25);

  RevoluteJointDef rjd = new RevoluteJointDef();
  rjd.bodyB = soporte.body;
  rjd.bodyA = barra.body;
  rjd.localAnchorA.x = 8;
  rjd.localAnchorA.y = 0;
  rjd.localAnchorB.x = 0;
  rjd.localAnchorB.y = 10;
  rjd.collideConnected = false;
  soporte_con_barra = (RevoluteJoint) box2d.world.createJoint(rjd);

  RevoluteJointDef rjd2 = new RevoluteJointDef();
  rjd2.bodyB = rueda1.body;
  rjd2.bodyA = frame.body;
  rjd2.initialize(rueda1.body, frame.body, rueda1.body.getWorldCenter());
  rjd2.collideConnected = false;
  rjd2.motorSpeed = PI*1.6;
  rjd2.maxMotorTorque = 1800.0;
  rjd2.enableMotor = false;
  rueda1_con_frame = (RevoluteJoint) box2d.world.createJoint(rjd2);

  RevoluteJointDef rjd3 = new RevoluteJointDef();
  rjd3.bodyB = rueda2.body;
  rjd3.bodyA = frame.body;
  rjd3.initialize(rueda2.body, frame.body, rueda2.body.getWorldCenter());
  rjd3.collideConnected = false;
  rjd3.motorSpeed = PI*1;
  rjd3.maxMotorTorque = 1800000.0;
  rjd3.enableMotor = false;
  rueda2_con_frame = (RevoluteJoint) box2d.world.createJoint(rjd3);


  WeldJointDef djd3 = new WeldJointDef();
  djd3.bodyA = peso.body;
  djd3.bodyB = barra.body;
  djd3.localAnchorA.x = 0;
  djd3.localAnchorA.y = 3;
  djd3.localAnchorB.x = 22;
  djd3.localAnchorB.y = 0;
  barra_con_peso = (WeldJoint) box2d.world.createJoint(djd3);

  RopeJointDef djd = new RopeJointDef();
  djd.bodyA = proyectil.body;
  djd.bodyB = barra.body;
  djd.localAnchorA.x = 0;
  djd.localAnchorA.y = 0;
  djd.localAnchorB.x = -23;
  djd.localAnchorB.y = 0;
  djd.maxLength = 30;
  proyectil_con_barra = (RopeJoint) box2d.world.createJoint(djd);

  WeldJointDef djd2 = new WeldJointDef();
  djd2.bodyA = proyectil.body;
  djd2.bodyB = soporte.body;
  djd2.localAnchorA.x = 0;
  djd2.localAnchorA.y = 2;
  djd2.localAnchorB.x = -2;
  djd2.localAnchorB.y = -7.5;
  proyectil_con_frame = (WeldJoint) box2d.world.createJoint(djd2);

  WeldJointDef weld = new WeldJointDef();
  weld.bodyA = frame.body;
  weld.bodyB = soporte.body;
  weld.localAnchorA.x = 0;
  weld.localAnchorA.y = 0;
  weld.localAnchorB.x = 0;
  weld.localAnchorB.y = -10;
  soporte_con_frame = (WeldJoint) box2d.world.createJoint(weld);

  WeldJointDef framepiso = new WeldJointDef();
  framepiso.bodyA = rueda1.body;
  framepiso.bodyB = piso.body;
  framepiso.initialize(rueda1.body, piso.body, rueda1.body.getWorldCenter());
  rueda1_con_piso = (WeldJoint) box2d.world.createJoint(framepiso);
  
  WeldJointDef framepiso2 = new WeldJointDef();
  framepiso2.bodyA = rueda2.body;
  framepiso2.bodyB = piso.body;
  framepiso2.initialize(rueda2.body, piso.body, rueda2.body.getWorldCenter());
  rueda2_con_piso = (WeldJoint) box2d.world.createJoint(framepiso2);
  
  r1 = new Ray(force, 0); //0
  r2 = new Ray(force, -force); //45
  r3 = new Ray(0, -force); //90
  r4 = new Ray(-force, -force); //135
  r5 = new Ray(-force, 0); //180
  r6 = new Ray(-force, force); //225
  r7 = new Ray(0, force); //270
  r8 = new Ray(force, force); //315
  
}

void mouseReleased() {
  if (mouseButton == LEFT){
    if (proyectil_con_frame != null) {
      box2d.world.destroyJoint(proyectil_con_frame);
      proyectil_con_frame = null;
    } else {
      WeldJointDef djd2 = new WeldJointDef();
      djd2.bodyA = proyectil.body;
      djd2.bodyB = soporte.body;
      djd2.localAnchorA.x = 0;
      djd2.localAnchorA.y = 2;
      djd2.localAnchorB.x = -2;
      djd2.localAnchorB.y = -7.5;
      proyectil_con_frame = (WeldJoint) box2d.world.createJoint(djd2);

      RopeJointDef djd = new RopeJointDef();
      djd.bodyA = proyectil.body;
      djd.bodyB = barra.body;
      djd.localAnchorA.x = 0;
      djd.localAnchorA.y = 0;
      djd.localAnchorB.x = -23;
      djd.localAnchorB.y = 0;
      djd.maxLength = 30;
      proyectil_con_barra = (RopeJoint) box2d.world.createJoint(djd);
    }
  }
  
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == CONTROL) {
      if (proyectil_con_barra != null) {
        box2d.world.destroyJoint(proyectil_con_barra);
        proyectil_con_barra = null;
      }
    }
    if (keyCode == ALT) {
      rueda2_con_frame.enableMotor(!rueda2_con_frame.isMotorEnabled());
      rueda1_con_frame.enableMotor(!rueda1_con_frame.isMotorEnabled());
      if (rueda1_con_piso != null && rueda2_con_piso != null ) {
          box2d.world.destroyJoint(rueda1_con_piso);
          rueda1_con_piso = null;
          box2d.world.destroyJoint(rueda2_con_piso);
          rueda2_con_piso = null;
      } else {
         WeldJointDef framepiso = new WeldJointDef();
         framepiso.bodyA = rueda1.body;
         framepiso.bodyB = piso.body;
         framepiso.initialize(rueda1.body, piso.body, rueda1.body.getWorldCenter());
         rueda1_con_piso = (WeldJoint) box2d.world.createJoint(framepiso);
         
         WeldJointDef framepiso2 = new WeldJointDef();
         framepiso2.bodyA = rueda2.body;
         framepiso2.bodyB = piso.body;
         framepiso2.initialize(rueda2.body, piso.body, rueda2.body.getWorldCenter());
         rueda2_con_piso = (WeldJoint) box2d.world.createJoint(framepiso2);
      }
    }
  }
}


void draw () {
  background(255);
  box2d.step();

  piso.display();

  soporte.display();

  barra.display();

  peso.display();

  frame.display(200,150,20);

  rueda1.display();
  rueda2.display();

  proyectil.display();
  
  r1.display(0); //0
  r2.display(45); //45
  r3.display(90); //90
  r4.display(135); //135
  r5.display(180); //180
  r6.display(225); //225
  r7.display(270); //270
  r8.display(315); //315
  
  
  r1.rayo(barra);
  r1.rayo(frame);
  r1.rayo(rueda1);
  r1.rayo(rueda2);
  r1.rayo(proyectil);
  r1.rayo(peso);
  
  r2.rayo(barra);
  r2.rayo(frame);
  r2.rayo(rueda1);
  r2.rayo(rueda2);
  r2.rayo(proyectil);
  r2.rayo(peso);
  
  r3.rayo(barra);
  r3.rayo(frame);
  r3.rayo(rueda1);
  r3.rayo(rueda2);
  r3.rayo(proyectil);
  r3.rayo(peso);
  
  r4.rayo(barra);
  r4.rayo(frame);
  r4.rayo(rueda1);
  r4.rayo(rueda2);
  r4.rayo(proyectil);
  r4.rayo(peso);
  
  r5.rayo(barra);
  r5.rayo(frame);
  r5.rayo(rueda1);
  r5.rayo(rueda2);
  r5.rayo(proyectil);
  r5.rayo(peso);
  
  r6.rayo(barra);
  r6.rayo(frame);
  r6.rayo(rueda1);
  r6.rayo(rueda2);
  r6.rayo(proyectil);
  r6.rayo(peso);
  
  r7.rayo(barra);
  r7.rayo(frame);
  r7.rayo(rueda1);
  r7.rayo(rueda2);
  r7.rayo(proyectil);
  r7.rayo(peso);
  
  r8.rayo(barra);
  r8.rayo(frame);
  r8.rayo(rueda1);
  r8.rayo(rueda2);
  r8.rayo(proyectil);
  r8.rayo(peso);
  
  if (proyectil_con_barra != null) {
      Vec2 v1 = new Vec2(0,0);
      Vec2 v2 = new Vec2(0,0);
      proyectil_con_barra.getAnchorA(v1);
      proyectil_con_barra.getAnchorB(v2);
      v1 = box2d.coordWorldToPixels(v1);
      v2 = box2d.coordWorldToPixels(v2);
      pushMatrix();
        fill(0,0,0);
        stroke(0,0,0);
        strokeWeight(1.5);
        line(v1.x, v1.y, v2.x, v2.y);
      popMatrix();
    }

  if (proyectil_con_frame != null) {
      Vec2 v3 = new Vec2(0,0);
      Vec2 v4 = new Vec2(0,0);
      proyectil_con_frame.getAnchorA(v3);
      proyectil_con_frame.getAnchorB(v4);
      v3 = box2d.coordWorldToPixels(v3);
      v4 = box2d.coordWorldToPixels(v4);
      pushMatrix();
      fill(0,0,0);
      stroke(200,0,200);
      strokeWeight(1.5);
      line(v3.x, v3.y, v4.x, v4.y);
      popMatrix();
    }
  
  if(mousePressed && mouseButton == RIGHT){
     r1.show = true;
     r2.show = true;
     r3.show = true;
     r4.show = true;
     r5.show = true;
     r6.show = true;
     r7.show = true;
     r8.show = true;
   }
   else{
     r1.show = false;
     r2.show = false;
     r3.show = false;
     r4.show = false;
     r5.show = false;
     r6.show = false;
     r7.show = false;
     r8.show = false;
   }
}
