class Peso {
  Body body;
  float x,y,w;
  
  Peso () {
    x = 540;
    y = 800;
    w = 25;
    
    BodyDef bd = new BodyDef();
    bd.type = BodyType.DYNAMIC;
    bd.position.set(box2d.coordPixelsToWorld(x, y));

    body = box2d.createBody(bd);
    body.setAngularDamping(1);

    PolygonShape ps = new PolygonShape();
    float bodyheight = box2d.scalarPixelsToWorld(w/2);
    ps.setAsBox(bodyheight, bodyheight);

    FixtureDef fd = new FixtureDef();
    fd.shape = ps;

    fd.density = 1000;
    fd.friction = 0.0;
    fd.restitution = 0.0;

    body.createFixture(fd);
  }
  
  void display() {
    Vec2 pos = box2d.getBodyPixelCoord(body);
    float a = body.getAngle();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(-a);
    noStroke();
    fill(255,0,0);
    rectMode(CENTER);
    rect(0, 0, w, w);
    popMatrix();
  }
}
