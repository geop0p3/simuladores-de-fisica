class Proyectil {
  Body body;
  float x,y,r;

  Proyectil () {
    x = 320;
    y = 800;
    r = 20;

    BodyDef bd = new BodyDef();
    bd.type = BodyType.DYNAMIC;
    bd.position.set(box2d.coordPixelsToWorld(x, y));

    body = box2d.createBody(bd);

    CircleShape cs = new CircleShape();
    cs.m_radius = box2d.scalarPixelsToWorld(r);

    FixtureDef fd = new FixtureDef();
    fd.shape = cs;

    fd.density = 20;
    fd.friction = 0.8;
    fd.restitution = 0.5;

    body.createFixture(fd);
  }

  void display() {
    Vec2 pos = box2d.getBodyPixelCoord(body);
    float a = body.getAngle();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(-a);
    noStroke();
    fill(0,0,255);
    ellipse(0,0,r*2,r*2);
    popMatrix();
  }
}
