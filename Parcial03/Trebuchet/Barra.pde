class Barra {
  Body body;
  float x,y,w,h;
  
  Barra () {
    x = 452;
    y = 710;
    w = 475;
    h = 10;
    
    BodyDef bd = new BodyDef();
    bd.type = BodyType.DYNAMIC;
    bd.position.set(box2d.coordPixelsToWorld(x, y));

    body = box2d.createBody(bd);

    PolygonShape ps = new PolygonShape();
    float bodyheight = box2d.scalarPixelsToWorld(w/2);
    float bodylength = box2d.scalarPixelsToWorld(h/2);
    ps.setAsBox(bodyheight, bodylength);

    FixtureDef fd = new FixtureDef();
    fd.shape = ps;

    fd.density = 1;
    fd.friction = 0.3;
    fd.restitution = 0.5;

    body.createFixture(fd);
  }
  
  void display() {
    Vec2 pos = box2d.getBodyPixelCoord(body);
    float a = body.getAngle();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(-a);
    noStroke();
    fill(255,150,0);
    rectMode(CENTER);
    rect(0, 0, w, h);
    popMatrix();
  }
}
