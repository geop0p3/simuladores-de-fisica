class Soporte {
  Body body;
  float x,y,w,h;

  Soporte () {
    x = 420;
    y = 780;
    w = 10;
    h = 200;

    BodyDef bd = new BodyDef();
    bd.type = BodyType.DYNAMIC;
    bd.position.set(box2d.coordPixelsToWorld(x, y));

    body = box2d.createBody(bd);

    PolygonShape ps = new PolygonShape();
    float bodyheight = box2d.scalarPixelsToWorld(w/2);
    float bodylength = box2d.scalarPixelsToWorld(h/2);
    ps.setAsBox(bodyheight, bodylength);

    FixtureDef fd = new FixtureDef();
    fd.shape = ps;

    fd.density = 20;
    fd.friction = 0;
    fd.restitution = 0;
    fd.isSensor = true;

    body.createFixture(fd);
  }

  void display() {
    Vec2 pos = box2d.getBodyPixelCoord(body);
    float a = body.getAngle();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(-a);
    fill(10,200,32);
    noStroke();
    rectMode(CENTER);
    rect(0, 0, w, h);
    popMatrix();
  }
}
