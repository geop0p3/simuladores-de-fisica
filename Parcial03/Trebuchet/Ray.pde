class Ray{
  RayCastInput inputRD;
  RayCastOutput outputRD;
  Vec2 c1D, c2D;
  Vec2 ray;
  Vec2 fuerza;
  boolean show;
  
  Ray(float x, float y){
    inputRD = new RayCastInput();
    outputRD = new RayCastOutput();
    c1D = new Vec2(0, 0);
    c2D = new Vec2(0, 0);
    inputRD.p1.set(box2d.coordPixelsToWorld(c1D.x,c1D.y)); // Cambiar a coordenadas de mouse
    
    fuerza = new Vec2(0,0);
    fuerza.x = x;
    fuerza.y = y;
    
    show = false;
  }
  
  void rayo(Barra obj)
  {
   float cercano=1;
   ray = c2D.sub(c1D);
   
    for(Fixture f = obj.body.getFixtureList(); f != null; f = f.getNext())
    {
      if(f.raycast(outputRD,inputRD, 1) )
      {
        if( outputRD.fraction < cercano )
        {
          cercano = outputRD.fraction;
          ray.mulLocal(cercano);
        }
        // Se ejecuta aqui cuando choca con raycast.
        rect(c1D.x + ray.x, c1D.y + ray.y , 10,10);
        Vec2 pos = obj.body.getWorldCenter();
        obj.body.applyForce(fuerza, pos);
        if (barra_con_peso != null) {
          box2d.world.destroyJoint(barra_con_peso);
          barra_con_peso = null;
        }
        if (soporte_con_barra != null) {
          box2d.world.destroyJoint(soporte_con_barra);
          soporte_con_barra = null;
        }
      } 
    
    }
  }
  
  void rayo(Frame obj)
  {
   float cercano=1;
   ray = c2D.sub(c1D);
   
    for(Fixture f = obj.body.getFixtureList(); f != null; f = f.getNext())
    {
      if(f.raycast(outputRD,inputRD, 1) )
      {
        if( outputRD.fraction < cercano )
        {
          cercano = outputRD.fraction;
          ray.mulLocal(cercano);
        }
        // Se ejecuta aqui cuando choca con raycast.
        rect(c1D.x + ray.x, c1D.y + ray.y , 10,10);
        Vec2 pos = obj.body.getWorldCenter();
        obj.body.applyForce(fuerza, pos);
        if (proyectil_con_frame != null) {
          box2d.world.destroyJoint(proyectil_con_frame);
          proyectil_con_frame = null;
        }
        if (soporte_con_frame != null) {
          box2d.world.destroyJoint(soporte_con_frame);
          soporte_con_frame = null;
        }
        if (rueda1_con_frame != null) {
          box2d.world.destroyJoint(rueda1_con_frame);
          rueda1_con_frame = null;
        }
        if (rueda2_con_frame != null) {
          box2d.world.destroyJoint(rueda2_con_frame);
          rueda2_con_frame = null;
        }
      } 
    
    }
  }
  
  void rayo(Piso obj)
  {
   float cercano=1;
   ray = c2D.sub(c1D);
   
    for(Fixture f = obj.body.getFixtureList(); f != null; f = f.getNext())
    {
      if(f.raycast(outputRD,inputRD, 1) )
      {
        if( outputRD.fraction < cercano )
        {
          cercano = outputRD.fraction;
          ray.mulLocal(cercano);
        }
        // Se ejecuta aqui cuando choca con raycast.
        rect(c1D.x + ray.x, c1D.y + ray.y , 10,10);
        Vec2 pos = obj.body.getWorldCenter();
        obj.body.applyForce(fuerza, pos);
      } 
    
    }
  }
  
  void rayo(Rueda obj)
  {
   float cercano=1;
   ray = c2D.sub(c1D);
   
    for(Fixture f = obj.body.getFixtureList(); f != null; f = f.getNext())
    {
      if(f.raycast(outputRD,inputRD, 1) )
      {
        if( outputRD.fraction < cercano )
        {
          cercano = outputRD.fraction;
          ray.mulLocal(cercano);
        }
        // Se ejecuta aqui cuando choca con raycast.
        rect(c1D.x + ray.x, c1D.y + ray.y , 10,10);
        Vec2 pos = obj.body.getWorldCenter();
        obj.body.applyForce(fuerza, pos);
        if (rueda1_con_piso != null) {
          box2d.world.destroyJoint(rueda1_con_piso);
          rueda1_con_piso = null;
        }
        if (rueda2_con_piso != null) {
          box2d.world.destroyJoint(rueda2_con_piso);
          rueda2_con_piso = null;
        }
      } 
    
    }
  }
  
  void rayo(Proyectil obj)
  {
   float cercano=1;
   ray = c2D.sub(c1D);
   
    for(Fixture f = obj.body.getFixtureList(); f != null; f = f.getNext())
    {
      if(f.raycast(outputRD,inputRD, 1) )
      {
        if( outputRD.fraction < cercano )
        {
          cercano = outputRD.fraction;
          ray.mulLocal(cercano);
        }
        // Se ejecuta aqui cuando choca con raycast.
        rect(c1D.x + ray.x, c1D.y + ray.y , 10,10);
        Vec2 pos = obj.body.getWorldCenter();
        obj.body.applyForce(fuerza, pos);
        if (proyectil_con_frame != null) {
          box2d.world.destroyJoint(proyectil_con_frame);
          proyectil_con_frame = null;
        }
        if (proyectil_con_barra != null) {
          box2d.world.destroyJoint(proyectil_con_barra);
          proyectil_con_barra = null;
        }
      } 
    
    }
  }
  
  void rayo(Peso obj)
  {
   float cercano=1;
   ray = c2D.sub(c1D);
   
    for(Fixture f = obj.body.getFixtureList(); f != null; f = f.getNext())
    {
      if(f.raycast(outputRD,inputRD, 1) )
      {
        if( outputRD.fraction < cercano )
        {
          cercano = outputRD.fraction;
          ray.mulLocal(cercano);
        }
        // Se ejecuta aqui cuando choca con raycast.
        rect(c1D.x + ray.x, c1D.y + ray.y , 10,10);
        Vec2 pos = obj.body.getWorldCenter();
        obj.body.applyForce(fuerza, pos);
      } 
    
    }
  }
  
  void display(float angle){
    inputRD.p2.set(box2d.coordPixelsToWorld(c2D.x,c2D.y)); // Sumarle a la coordenada de mouse la distancia que quieras, ejemplo mouse.x + 100
    inputRD.maxFraction = 1;
    
    PVector p = PVector.fromAngle(radians(-angle));
    
    p.mult(50);
    
    if(show){
      c1D.x = mouseX;
      c1D.y = mouseY;
      c2D.x = mouseX + p.x; // Se le suma la distancia al centro del raycast
      c2D.y = mouseY + p.y;
      
      line(c1D.x, c1D.y, c2D.x, c2D.y);
    }
    else{
      c1D.x = -10;
      c1D.y = -10;
      c2D.x = -10;
      c2D.y = -10;
    }
  }
}
