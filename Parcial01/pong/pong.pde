//
//   _____                _____ _           _       _
//  |  _  |___ ___ ___   |   __|_|_____ _ _| |___ _| |___ ___ ___ ___
//  |   __| . |   | . |  |__   | |     | | | | .'| . | . |  _| -_|_ -|
//  |__|  |___|_|_|_  |  |_____|_|_|_|_|___|_|__,|___|___|_| |___|___|
//                |___|
//                                                       Version 0.0.1
//
//                           Carlos Flores (Carlosflores@hedronmx.com)
//
int score1 = 0;
int score2 = 0;
float py1 = 240;
int px1 = 800 - 25;
int px2 = 15;
int p2spd = 9;
int p1spd = 9;
float py2 = 240;
float bx = 390;
float by = 300;
float spdx = 4;
float spdy = 4;

void setup() {
    size(800, 600);
    noSmooth();
    frameRate(60);
}

void draw() {
    background(0, 0, 0);
    // Paddle izquierda
    rect(px2, py2, 10, 100);
    // Linea central
    rect(width / 2 - 2, 0, 4, height);
    // Paddle derecha
    rect(px1, py1, 10, 100);

    // Score Texto
    textSize(64);
    text(score1, width / 2 - 80, 60);
    text(score2, width / 2 + 35, 60);


    // Ball
    square(bx, by, 20);
    bx = bx + spdx;
    by = by + spdy;

    //bounce wall top
    if (by < 0) {
        spdy = spdy * -1;
    }

    // bounce wall bottom
    if (by >= 580) {
        spdy = spdy * -1;
    }

    // bounce paddle izquierda
    if (by >= py2 && by <= py2 + 100 && bx >= px2 && bx <= px2 + 10) {
        spdx = spdx * -1;
        spdx += 0.4;
    }
    // bounce paddle derecha
    if (by >= py1 && by <= py1 + 100 && bx >= px1 - 10 && bx <= px1) {
        spdx = spdx * -1;
        spdx -= 0.4;
    }

    //score y ball reset
    if (bx > 800) {
        score1++;
        bx = 390;
        by = 300;
        spdx = -4;
    };
    if (bx < 0) {
        score2++;
        bx = 390;
        by = 300;
        spdx = 4;
    };

    // Movimiento

    // Control Izquierda
    if (keyPressed == true) {
        if (key == 'w' && py2 > 0) {
            py2 = py2 - p2spd;
        } else if (key == 's' && py2 < 500) {
            py2 = py2 + p2spd;
        }
    };

    // Control Derecha
    if (keyPressed == true) {
        if (keyCode == UP && py1 > 0) {
            py1 = py1 - p1spd;
        } else if (keyCode == DOWN && py1 < 500) {
            py1 = py1 + p1spd;
        }
    };
}
