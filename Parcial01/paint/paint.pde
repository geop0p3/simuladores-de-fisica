import shiffman.box2d.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;
import org.jbox2d.dynamics.*;

int mode = 0;
int x = pmouseX;
int y = pmouseY;
void setup() {
	size (640, 360);
	background(230);
	smooth();
}

void draw () {
	if ( mouseButton == LEFT && mode==0){
		stroke(120);
		line(mouseX, mouseY, pmouseX, pmouseY);
	}
	if ( mouseButton == LEFT && mode==1){
		rectMode(CORNERS);
		fill(20);
		rect(pmouseX, pmouseY, mouseX, mouseY);
	}
	if ( mouseButton == LEFT && mode==2){
		ellipseMode(CORNERS);
		fill(230);
		ellipse(x, y, mouseX, mouseY);
	}
}

void keyPressed() {
	if (key == 'l') {
		mode = 0;
	}
	if (key == 'r') {
		mode = 1;
	}
	if (key == 'e'){
		mode = 2;
	}
}

void mouseClicked() {
	print(mode);
	y=pmouseY;
	x=pmouseX;
}

